#include <stdio.h>

void decompress(FILE *fp1, FILE *fp2)
{
	int count, data_c;
	while ((count = fgetc(fp1)) != EOF) {
		data_c = fgetc(fp1);
		for (int i = 0; i < count; i++)
			fputc(data_c, fp2);
	}
}

int main(void)
{
	decompress(stdin, stdout);
	fclose(stdin);
	fclose(stdout);
	return 0;
}