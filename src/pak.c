#include <stdio.h>

void compress(FILE *fp1, FILE *fp2)
{
	int data_c, flag;
	int count = 0;
	flag = fgetc(fp1);
	fseek(fp1, 0, 0);
	while ((data_c = fgetc(fp1)) != EOF) {
		if ((flag == data_c) && (count < 255)) {
			count++;
			continue;
		}
		fputc(count, fp2);
		fputc(flag, fp2);
		flag = data_c;
		count = 1;
	}
	fputc(count, fp2);
	fputc(flag, fp2);
}

int main(void)
{
	compress(stdin, stdout);
	fclose(stdin);
	fclose(stdout);
	return 0;
}