.PHONY: clean test all
all: pak upak
pak: ./src/pak.c bin
	gcc -g ./src/pak.c -o ./bin/pak
upak: ./src/upak.c bin
	gcc -g ./src/upak.c -o ./bin/upak
bin:
	mkdir bin
clean:
	rm -rf ./bin
	rm -rf ./test
	rm -rf ./pak
	rm -rf ./upak
	rm upak.deb
	rm pak.deb

test: clean pak upak
	@mkdir test
	@echo "aaaaaaaaaallllllllllrrrrrrrjjjjjjjkkkkkk11122223334445667777...." > ./test/test_s
	@echo "qqqqqqqqqqqppppppppppprrrrrrrrr3333333300000000===';;;...,,,,llll" >> ./test/test_s
	./bin/pak < ./test/test_s > ./test/test_compress
	./bin/upak < ./test/test_compress > ./test/test_decompress
	diff ./test/test_s ./test/test_decompress
install:all
	@mkdir -p pak/DEBIAN
	@echo "Package: pak" > pak/DEBIAN/control
	@echo "Version: 1.0" >> pak/DEBIAN/control
	@echo "Section: custom" >> pak/DEBIAN/control
	@echo "Priority: optional" >> pak/DEBIAN/control
	@echo "Architecture: all" >> pak/DEBIAN/control
	@echo "Essential: no" >> pak/DEBIAN/control
	@echo "Installed-Size: 1024" >> pak/DEBIAN/control
	@echo "Maintainer: Vlad Kudenchuk" >> pak/DEBIAN/control
	@echo "Description: Simple file compressor RLE" >> pak/DEBIAN/control
	@mkdir -p pak/usr/bin
	@cp bin/pak pak/usr/bin/pak
	dpkg-deb --build pak
	sudo dpkg -i pak.deb

	@mkdir -p upak/DEBIAN
	@echo "Package: upak" > upak/DEBIAN/control
	@echo "Version: 1.0" >> upak/DEBIAN/control
	@echo "Section: custom" >> upak/DEBIAN/control
	@echo "Priority: optional" >> upak/DEBIAN/control
	@echo "Architecture: all" >> upak/DEBIAN/control
	@echo "Essential: no" >> upak/DEBIAN/control
	@echo "Installed-Size: 1024" >> upak/DEBIAN/control
	@echo "Maintainer: Vlad Kudenchuk" >> upak/DEBIAN/control
	@echo "Description: Simple file compressor RLE" >> upak/DEBIAN/control
	@mkdir -p upak/usr/bin
	@cp bin/upak upak/usr/bin/upak
	dpkg-deb --build upak
	sudo dpkg -i upak.deb

uninstall:
	@sudo dpkg --remove pak
	@sudo dpkg --remove upak
